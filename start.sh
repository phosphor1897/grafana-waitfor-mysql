#!/bin/bash
echo "Waiting for postgres"
until $(nc -zv $GF_DATABASE_HOST 5432); do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up"

sleep 1

psql -h $GF_DATABASE_HOST -U $GF_DATABASE_USER -c "CREATE DATABASE $GF_DATABASE_NAME"

exec /usr/sbin/grafana-server "--homepath=/usr/share/grafana" "--config=/etc/grafana/grafana.ini" "cfg:default.log.mode=console" "cfg:default.paths.data=/var/lib/grafana" "cfg:default.paths.logs=/var/log/grafana" "cfg:default.paths.plugins=/var/lib/grafana/plugins" "cfg:default.paths.provisioning=/etc/grafana/provisioning"
